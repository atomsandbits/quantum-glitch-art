#!/bin/bash
cd $(dirname $0) && cd ../..

rm -rf ./images/quantum/src
cp -r ../src ./images/quantum/src
(cd ./images/quantum/src && meteor reset >/dev/null)

# Load Environment Variables
set -a
source variables.default.env
if [ -f variables.env ]; then
    source variables.env
fi

# Run Docker Compose
docker-compose build --build-arg GITHUB_TOKEN=$GITHUB_TOKEN quantum
rm -rf ./images/quantum/src
