#!/bin/bash
cd $(dirname $0) && cd ..

# Load Environment Variables
set -a
source variables.default.env
if [ -f variables.env ]; then
    source variables.env
fi

# Build Images
./scripts/docker/build-image.sh quantum
docker-compose build mongo
