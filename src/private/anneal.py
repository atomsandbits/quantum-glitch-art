import argparse
import base64
import io
import os
import json
import sys
from PIL import Image
from dwave_sapi2.core import async_solve_qubo, await_completion
from dwave_sapi2.remote import RemoteConnection
from dwave_sapi2.util import ising_to_qubo
import matplotlib.pyplot as plt
import numpy as np

# -----------------------
#     Argument Parser
# -----------------------

parser = argparse.ArgumentParser()
parser.add_argument('--image-data', help='base64 png image')
parser.add_argument('--colormap', help='matplotlib colormap')
parser.add_argument('--coupling-type', help='coupling type')
parser.add_argument('--coupling-steps', help='number of coupling steps')
parser.add_argument(
    '--final-coupling-value', help='final coupling value to reach')
parser.add_argument('--number-of-reads', help='number of reads on each anneal')
results, remaining = parser.parse_known_args()

image_data = results.image_data
colormap = results.colormap
coupling_type = results.coupling_type
coupling_steps = int(results.coupling_steps)
final_coupling_value = float(results.final_coupling_value)
num_reads = int(results.number_of_reads)

print(coupling_type)
print(coupling_steps)
print(final_coupling_value)

im = Image.open(io.BytesIO(base64.b64decode(image_data)))
color_channels = im.split()
red = color_channels[0]
green = color_channels[1]
blue = color_channels[2]

sys.argv = [sys.argv[0]]

# -----------------------
#     Anneal
# -----------------------

url = 'https://cloud.dwavesys.com/sapi'
token = os.environ.get('DWAVE_TOKEN')
print('token')
print(token)
if not token:
    raise Exception('DWAVE_TOKEN required, exiting...')
conn = RemoteConnection(url, token)
print(conn.solver_names())

solver = conn.get_solver("DW_2000Q_VFYC_2")
nqubits = len(solver.properties['qubits'])
ncouplers = len(solver.properties['couplers'])

print(nqubits)

# red_max = np.amax(red)
# blue_max = np.amax(blue)
# green_max = np.amax(green)
# if (red_max > blue_max and red_max > green_max):
#     channel = red
# elif (blue_max > green_max and blue_max > red_max):
#     channel = blue
# else:
#     channel = green

json_data = {
    'images': {
        'red': [],
        'blue': [],
        'green': [],
    }
}
for channel_index, channel in enumerate([red, blue, green]):
    for coupling in np.linspace(0., final_coupling_value, coupling_steps):
        gbg = np.array(channel)

        im = (np.round(gbg / 63.75) - 2) / 2
        gbg2 = im

        # plt.imshow(gbg2.astype(np.float), cmap=colormap)

        print(np.unique(gbg2.flatten()))

        J = [coupling for X in range(ncouplers)]
        if (coupling_type == 'checkered'):
            for index, value in enumerate(J):
                if index % 2 == 0:
                    J[index] = 0
        (Q, qubo_offset) = ising_to_qubo(gbg2.flatten(), {
            tuple(X): J[i]
            for i, X in enumerate(solver.properties['couplers'])
        })
        anneal_schedule = [[0, 0], [100, 1]]
        submitted_problem = async_solve_qubo(solver, Q, num_reads=num_reads)
        await_completion([submitted_problem], 1, float('inf'))
        res = submitted_problem.result()
        averaged_res = (np.sum(np.array(res['solutions']), axis=0)[:1936]
                        ).reshape(44, 44).astype(np.float) / num_reads
        print(np.unique(averaged_res))
        plt.imshow(averaged_res, cmap=colormap)
        plt.axis('off')
        buf = io.BytesIO()
        plt.savefig(
            buf,
            format='png',
            bbox_inches='tight',
            pad_inches=0,
            transparent=True)
        img_base64 = base64.b64encode(buf.getvalue())
        if (channel_index == 0):
            json_data['images']['red'].append(img_base64)
        elif (channel_index == 1):
            json_data['images']['blue'].append(img_base64)
        else:
            json_data['images']['green'].append(img_base64)
        plt.close()
        buf.close()

print('~~~ JSON Data ~~~')
print(json.dumps(json_data))
