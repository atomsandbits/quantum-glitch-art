import { spawn } from "child_process";

const runAnnealing = ({
  image,
  colormap,
  couplingSteps,
  couplingType,
  numReads,
  finalCouplingValue
}) => {
  return new Promise((resolve, reject) => {
    const job = spawn(
      "python2.7",
      [
        "anneal.py",
        "--image-data",
        image,
        "--colormap",
        colormap,
        "--coupling-steps",
        couplingSteps,
        "--coupling-type",
        couplingType,
        "--final-coupling-value",
        finalCouplingValue,
        "--number-of-reads",
        numReads
      ],
      {
        cwd: `assets/app`
      }
    );

    let output = "";
    /* Process Callbacks */
    job.stdout.on("data", data => {
      output += data.toString() + "\n";
    });
    job.stderr.on("data", data => {
      output += data.toString() + "\n";
    });
    /* Calculation Errored Out */
    job.on("error", error => {
      throw error;
    });
    /* Calculation Finished */
    job.on("exit", (code, error) => {
      if (isNaN(code)) code = -1;
      console.log(`Finished Anneal...`);
      console.log(`Exit Code: ${code}`);
      const split_output = output.split("~~~ JSON Data ~~~");
      console.log(split_output[0]);
      const data = JSON.parse(split_output[1].replace(/[\u0000-\u0019]+/g, ""));
      resolve(data.images);
    });
  });
};

export default runAnnealing;
