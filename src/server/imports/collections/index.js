
import { Mongo } from 'meteor/mongo';

const AnnealResults = new Mongo.Collection('anneal-results');

export { AnnealResults };
