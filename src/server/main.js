import { Meteor } from "meteor/meteor";
import { onPageLoad } from "meteor/server-render";
import sharp from "sharp";
import { AnnealResults } from "/server/imports/collections";
import runAnnealing from "/server/imports/run-annealing";

Meteor.startup(() => {
  // Code to run on server startup.
  console.log(`Greetings from ${module.id}!`);
});

Meteor.methods({
  downsample: async image => {
    const buffer = await sharp(new Buffer(image, "base64"))
      .resize(44, 44)
      .toBuffer();
    return buffer.toString("base64");
  },
  glitch: async ({
    image,
    couplingType,
    couplingSteps,
    colormap,
    finalCouplingValue,
    numReads
  }) => {
    if (!couplingType) couplingType = "flat";
    if (!colormap) colormap = "cool";
    if (!finalCouplingValue) finalCouplingValue = 0.4;
    if (!couplingSteps) couplingSteps = 5;
    if (couplingSteps > 10) couplingSteps = 10;
    if (!numReads) numReads = 100;
    if (numReads > 500) numReads = 500;
    const buffer = await sharp(new Buffer(image, "base64"))
      .resize(44, 44)
      .toBuffer();
    const base64 = buffer.toString("base64");
    const parameters = {
      image: base64,
      colormap,
      couplingSteps,
      couplingType,
      numReads,
      finalCouplingValue
    };
    const previousResult = AnnealResults.findOne({
      parameters
    });
    if (previousResult) return previousResult.images;
    const result = await runAnnealing({
      ...parameters
    });
    AnnealResults.insert({
      parameters,
      images: result
    });
    return result;
  }
});

onPageLoad(sink => {
  // Code to run on every request.
  sink.renderIntoElementById(
    "server-render-target",
    `Server time: ${new Date()}`
  );
});
