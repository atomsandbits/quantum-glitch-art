import styled, { injectGlobal } from "styled-components";

injectGlobal`
  html, body {
    background-color: white;
    color: #404040;
    font-family: Gaegu, Roboto, Helvetica, Arial;
    margin: 0; padding: 0;
  }
  img {
    image-rendering: pixelated;
    padding: 10px;
  }
  a {
    color: #4ECDC4
    text-decoration: none;
  }
`;

export const Link = styled.a`

`;

export const Title = styled.h1`
  width: 100%;
  text-align: center;
  font-size: 38px;
  margin-bottom: 10px;
`;
export const Description = styled.div`
  width: 100%;
  text-align: center;
  font-size: 26px;
  margin-bottom: 30px;
`;

export const PageContainer = styled.div`
  display: flex;
  margin: 0 auto;
  max-width: 1200px;
  padding: 0 30px 30px;
  flex-wrap: wrap;
  justify-content: center;
  position: relative;
  width: 100%;
  box-sizing: border-box;
  overflow: hidden;
`;

export const BottomSection = styled.div`
  border-top: 1px dotted gainsboro;
  margin: 30px 0;
  width: 100%;
`;

export const LeftSectionContainer = styled.div`
  display: flex;
  max-width: 600px;
  flex-wrap: wrap;
  justify-content: center;
`;

export const BaseImageContainer = styled.div`
  border: 1px solid gainsboro;
  overflow: hidden;
  flex-shrink: 0;
`;

export const BaseImage = styled.img`
  width: 75vw;
  max-width: 360px;
`;

export const GlitchedImagesContainer = styled.div`
  padding: 0 20px;
  display: flex;
  flex-direction: column;
  @media (max-width: 900px) {
    margin-top: 30px;
  }
`;

export const GlitchedImage = styled.img`
  width: 180px;
  margin-bottom: 30px;
  padding: 0;
`;

export const GlitchedPlaceholder = styled.div`
  height: 180px;
  width: 180px;
  margin-bottom: 30px
  background-color: gainsboro;
`;

export const SampleImagesContainer = styled.div`
  width: 100%;
  justify-content: center;
`;

export const SampleImage = styled.img`
  border: 1px solid gainsboro;
  margin: 15px;
  height: 20vw;
  max-height: 100px;
  width: 20vw;
  max-width: 100px;
`;

export const ButtonBase = styled.label`
  background-color: #404040;
  font-weight: 700;
  display: inline-block;
  border-radius: 2px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  color: #f7fff7;
  cursor: pointer;
  padding: 15px 20px;
  margin: 0 20px;
  width: 100px;
  text-align: center;
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  &:hover {
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  }
`;

export const GlitchButtonContainer = styled.div`
  width: 100%;
  text-align: right;
  margin-top: 20px;
`;
export const GlitchButton = styled(ButtonBase)`
  background-color: #e04251;
  cursor: ${({ loading }) => (loading ? "default" : "cursor")};
  div {
    margin: 0 auto;
  }
  &:hover {
    box-shadow: ${({ loading }) =>
      loading
        ? "0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24)"
        : "0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)"};
  }
`;

export const ChooseFileInput = styled.input`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
`;

export const ChooseFileLabel = styled(ButtonBase)``;

export const WebcamButton = styled(ButtonBase)``;

export const AdditionalOptions = styled.div`
  margin: 20px auto 0;
  display: flex;
  flex-wrap: wrap;
  max-width: 800px;
  justify-content: center;
  align-items: center;
  div {
    padding: 15px 15px;
    display: block;
  }
`;

export const Footer = styled.div`
  align-items: center;
  background-color: #404040;
  color: white;
  display: flex;
  font-size: 18px;
  justify-content: center;
  min-height: 60px;
  width: 100%;
  padding: 0 20px;
  box-sizing: border-box;
  text-align: center;
`;

export const FooterLink = styled.a`
  color: white;
  font-weight: bold;
`;
