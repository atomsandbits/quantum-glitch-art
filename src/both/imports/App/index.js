import React, { Fragment } from "react";
import {
  compose,
  withHandlers,
  withState,
  withProps,
  lifecycle,
  pure
} from "recompose";
import { Meteor } from "meteor/meteor";
import Webcam from "webcamjs";
import { CircleLoader } from "react-spinners";

import sampleImages from "./images";
import {
  Title,
  Description,
  Link,
  PageContainer,
  LeftSectionContainer,
  BaseImageContainer,
  BaseImage,
  GlitchedImagesContainer,
  GlitchedImage,
  GlitchedPlaceholder,
  SampleImagesContainer,
  SampleImage,
  GlitchButtonContainer,
  GlitchButton,
  ChooseFileInput,
  ChooseFileLabel,
  WebcamButton,
  BottomSection,
  AdditionalOptions,
  Footer,
  FooterLink
} from "./styles";

let glitchInterval;
const enhance = compose(
  withState("loading", "setLoading", false),
  withState("baseImage", "setBaseImage", sampleImages[0]),
  withState("glitchedImages", "setGlitchedImages", null),
  withState("glitchIndex", "setGlitchIndex", 0),
  withState("couplingType", "setCouplingType", "checkered"),
  withState("finalCouplingValue", "setFinalCouplingValue", 0.4),
  withState("couplingSteps", "setCouplingSteps", 5),
  withState("numReads", "setNumReads", 100),
  withState("colormap", "setColormap", "cool"),
  withState("animationSpeed", "setAnimationSpeed", 200),
  withProps(
    ({ glitchedImages, glitchIndex, setGlitchIndex, animationSpeed }) => {
      clearInterval(glitchInterval);
      if (glitchedImages) {
        glitchInterval = setInterval(() => {
          glitchIndex += 1;
          if (glitchIndex > glitchedImages.red.length - 1) {
            glitchIndex = 0;
          }
          setGlitchIndex(glitchIndex);
        }, animationSpeed);
      }
    }
  ),
  withHandlers({
    fetchBaseImage: ({ setBaseImage, setGlitchedImages }) => image => {
      Meteor.call("downsample", image, (error, smallerImage) => {
        setGlitchedImages(null);
        setBaseImage(smallerImage);
      });
    }
  }),
  withHandlers({
    takeSelfie: ({ fetchBaseImage, setGlitchedImages }) => event => {
      setGlitchedImages(null);
      setTimeout(() => {
        Webcam.attach("#first-placeholder");
        setTimeout(() => {
          Webcam.snap(data_uri => {
            fetchBaseImage(data_uri.split("data:image/jpeg;base64,")[1]);
          });
          Webcam.reset();
        }, 3000);
      }, 100);
    },
    getFile: ({ fetchBaseImage }) => event => {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        const base64 = reader.result;
        if (base64.split("data:image/jpeg;base64,").length > 1) {
          fetchBaseImage(base64.split("data:image/jpeg;base64,")[1]);
        } else if (base64.split("data:image/png;base64,")[1]) {
          fetchBaseImage(base64.split("data:image/png;base64,")[1]);
        } else {
          alert("Only JPGs and PNGs!");
        }
      };
    },
    selectImage: ({ fetchBaseImage }) => event => {
      const image = event.target.src.split("data:image/png;base64,")[1];
      fetchBaseImage(image);
    },
    submitGlitch: ({
      baseImage,
      setGlitchedImages,
      couplingType,
      colormap,
      numReads,
      couplingSteps,
      finalCouplingValue,
      setLoading
    }) => event => {
      setLoading(true);
      Meteor.call(
        "glitch",
        {
          image: baseImage,
          couplingType,
          colormap,
          numReads,
          couplingSteps,
          finalCouplingValue
        },
        (error, data) => {
          console.log(data);
          setGlitchedImages(data);
          setLoading(false);
        }
      );
    }
  }),
  pure
);

const AppPure = ({
  baseImage,
  colormap,
  couplingType,
  fetchBaseImage,
  getFile,
  glitchedImages,
  glitchIndex,
  loading,
  selectImage,
  setColormap,
  setCouplingType,
  submitGlitch,
  takeSelfie,
  numReads,
  setNumReads,
  couplingSteps,
  setCouplingSteps,
  finalCouplingValue,
  setFinalCouplingValue,
  setAnimationSpeed,
  animationSpeed
}) => (
  <Fragment>
    <PageContainer>
      <Title>quantum glitch art</Title>
      <Description>
        glitchy art made with the help of quantum annealing <br />{" "}
        <span style={{ fontSize: 20 }}>
          (on the{" "}
          <Link href="https://www.dwavesys.com/d-wave-two-system">
            D-Wave 2000Q
          </Link>)
        </span>
      </Description>
      <LeftSectionContainer>
        <BaseImageContainer>
          <BaseImage
            id="base-image"
            src={`data:image/png;base64,${baseImage}`}
          />
        </BaseImageContainer>
        <SampleImagesContainer style={{ display: "flex" }}>
          {sampleImages.map((image, index) => (
            <SampleImage
              key={`selectable-image-${index}`}
              src={`data:image/png;base64,${image}`}
              onClick={selectImage}
            />
          ))}
        </SampleImagesContainer>
        <ChooseFileInput
          type="file"
          id="file"
          onChange={loading ? () => {} : getFile}
          accept="image/*"
        />
        <ChooseFileLabel htmlFor="file">upload</ChooseFileLabel>
        <WebcamButton onClick={loading ? () => {} : takeSelfie}>selfie</WebcamButton>
        <GlitchButtonContainer>
          <GlitchButton onClick={loading ? () => {} : submitGlitch} loading={loading}>
            {loading ? (
              <CircleLoader color={"white"} size={25} loading={true} />
            ) : (
              "gLiTCH"
            )}
          </GlitchButton>
        </GlitchButtonContainer>
      </LeftSectionContainer>
      <GlitchedImagesContainer>
        {glitchedImages ? (
          <Fragment>
            <GlitchedImage
              src={`data:image/png;base64,${glitchedImages.red[glitchIndex]}`}
            />
            <GlitchedImage
              src={`data:image/png;base64,${glitchedImages.blue[glitchIndex]}`}
            />
            <GlitchedImage
              src={`data:image/png;base64,${glitchedImages.green[glitchIndex]}`}
            />
          </Fragment>
        ) : (
          <Fragment>
            <GlitchedPlaceholder id="first-placeholder" />
            <GlitchedPlaceholder />
            <GlitchedPlaceholder />
          </Fragment>
        )}
      </GlitchedImagesContainer>
      <BottomSection>
        <AdditionalOptions>
          <div>
            <label>
              coupling type:
              <select
                value={couplingType}
                onChange={event => setCouplingType(event.target.value)}
              >
                <option value="flat">flat</option>
                <option value="checkered">checkered</option>
              </select>
            </label>
          </div>

          <div>
            <label>
              colormap:
              <select
                value={colormap}
                onChange={event => setColormap(event.target.value)}
              >
                <option value="cool">cool</option>
                <option value="spring">spring</option>
                <option value="autumn">autumn</option>
                <option value="plasma">plasma</option>
                <option value="RdYlBu">RdYlBu</option>
                <option value="seismic">seismic</option>
                <option value="hsv">hsv</option>
                <option value="gist_rainbow">gist rainbow</option>
              </select>
            </label>
          </div>

          <div>
            <label>
              reads:
              <input
                type="number"
                value={numReads}
                max={300}
                min={1}
                onChange={event => setNumReads(event.target.value)}
              />
            </label>
          </div>
          <div>
            <label>
              coupling steps:
              <input
                type="number"
                value={couplingSteps}
                max={10}
                min={2}
                step={1}
                onChange={event => setCouplingSteps(event.target.value)}
              />
            </label>
          </div>

          <div>
            <label>
              final coupling value:
              <input
                type="number"
                value={finalCouplingValue}
                max={1}
                min={0.01}
                step="0.1"
                onChange={event => setFinalCouplingValue(event.target.value)}
              />
            </label>
          </div>

          <div>
            <label>
              animation speed:
              <input
                type="number"
                value={animationSpeed}
                max={10000}
                min={100}
                step={100}
                onChange={event => setAnimationSpeed(event.target.value)}
              />
            </label>
          </div>
        </AdditionalOptions>
      </BottomSection>
    </PageContainer>
    <Footer>
      <div>
        {"© 2018 "}
        <FooterLink href="https://www.atomsandbits.ai">Atoms+Bits</FooterLink>
        {" | "}
        <FooterLink
          style={{ color: "#4ECDC4" }}
          href="https://gitlab.com/atomsandbits/quantum-glitch-art"
        >
          source
        </FooterLink>{" "}
        | Shout out to{" "}
        <FooterLink href="https://www.dwavesys.com/home">D-Wave</FooterLink>
      </div>
    </Footer>
  </Fragment>
);

const App = enhance(AppPure);

export default App;
